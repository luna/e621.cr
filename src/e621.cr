require "http/client"
require "json"

module E621
  VERSION = "0.1.0"
  E621_BASE = "https://e621.net/post/index.json?"
  HYPNOHUB_BASE = "https://hypnohub.net/post/index.json?"

  class Error < Exception
  end

  class Booru
    def initialize(@base_url : String)
    end

    def search(tags = [] of String, opts = {} of Symbol => String)
      encoded_tags = tags.map do |tag|
        HTTP.quote_string tag
      end

      joined_tags = encoded_tags.join "&"

      kwargs = opts.map do |key, value|
        key_s = HTTP.quote_string key.to_s
        value_s = HTTP.quote_string value.to_s

        "&#{key_s}=#{value_s}"
      end

      response = HTTP::Client.get "#{@base_url}&tags=#{joined_tags}"

      if response.status_code != 200
        raise Error.new "meow"
      end

      JSON.parse response.body
    end
  end

  def e621
    Booru.new E621_BASE
  end

  def hypnohub
    Booru.new 
  end
end
