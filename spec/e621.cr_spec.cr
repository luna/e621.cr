require "./spec_helper"

describe E621 do
  it "e621 - raw" do
    e6 = E621::Booru.new E621::E621_BASE
    e6.search().size > 0
  end

  it "e621 - single tag" do
    e6 = E621::Booru.new E621::E621_BASE
    e6.search(["stick"]).size > 0
  end

  it "e621 - single post" do
    e6 = E621::Booru.new E621::E621_BASE
    e6.search([] of String, {limit: 1}).size == 1
  end

  it "hh - raw" do
    hh = E621::Booru.new E621::HYPNOHUB_BASE
    hh.search().size > 0
  end

  it "hh - single tag" do
    hh = E621::Booru.new E621::HYPNOHUB_BASE

    # Girls........
    hh.search(["female"]).size > 0
  end

  it "hh - single post" do
    hh = E621::Booru.new E621::HYPNOHUB_BASE
    hh.search([] of String, {limit: 1}).size == 1
  end

end
