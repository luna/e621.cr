# e621.cr

Crystal for Degenerates

## Installation

Add this to your application's `shard.yml`:

```yaml
dependencies:
  e621:
    gitlab: luna/e621.cr
```

## Usage

```crystal
require "e621"

# instantiate a new booru client for e621
# you can use E621::HYPNOHUB_BASE as well
e6 = E621::Booru.new E621::E621_BASE

# search as you wish!

# get all possible posts from the index page
e6.search()

# get posts with tags
e6.search(["female", "wolf"])

# get 2 posts max
e6.search([] of String, {limit: 2})
```

## Development

TODO: Write development instructions here

## Contributing

1. Fork it (<https://gitlab.com/luna/e621.cr/forks/new>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [luna](https://gitlab.com/luna) Luna Mendes - creator, maintainer
